# SNAP #
### A docker image for the [SNAP](https://rostlab.org/owiki/index.php/Snap) variant effect predictor ([RostLab & BrombergLab](https://www.bromberglab.org/project/snap/)). ###

SNAP is a method for evaluating effects of single amino acid substitutions on protein function. It was developed by Yana Bromberg at the Rost Lab, at Columbia University, New York. Single Nucleotide Polymorphisms (SNPs) represent a very large portion of all genetic variations. SNPs found in the coding regions of genes are often non-synonymous, changing a single amino acid in the encoded protein sequence. SNPs are either “neutral” in the sense that the resulting point-mutated protein is not functionally discernible from the wild-type, or they are “non-neutral” in that the mutant and wild-type differ in function. The ability to identify non-neutral substitutions in an ocean of SNPs could significantly aid targeting disease causing detrimental mutations, as well as SNPs that increase the fitness of particular phenotypes. SNAP is a neural-network based method that uses in silico derived protein information (e.g. secondary structure, conservation, solvent accessibility, etc.) in order to make predictions regarding functionality of mutated proteins. The network takes protein sequences and lists of mutants as input, returning a score for each substitution. These scores can then be translated into binary predictions of effect (present/absent) and reliability indices (RI).

## Pre-Requirements ##

Docker

**Dependencies**

* PredictProtein Database ([http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db](http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db))

## Usage ##

** Docker **

The SNAP docker image can be run in various ways:

Interactive:
```
$ docker run --rm -it -v <LOCAL_INPUT_DIRECTORY>:/input -v <LOCAL_OUTPUT_DIRECTORY>:/output bromberglab/snap [OPTIONS]
```

Capsulated:
```
$ docker run --rm -v <LOCAL_INPUT_DIRECTORY>:/input -v <LOCAL_OUTPUT_DIRECTORY>:/output bromberglab/snap [OPTIONS]
```

To mount a downloaded database add the following argument:
```
-v <LOCAL_DATA_DIRECTORY>:/usr/share/rostlab-data/data/
```

To download the latest release of the database run:
```
$ docker run -t -d --name=my_snap -v <LOCAL_INPUT_DIRECTORY>:/input -v <LOCAL_OUTPUT_DIRECTORY>:/output bromberglab/snap
$ docker exec my_snap setupdb
```

Then either attach to the running container to run *snap* via an interactive session
```
$ docker exec -i -t my_snap /bin/bash
```
or execute *snap* directly
```
$ docker exec my_snap snapfun [OPTIONS]

```

Finally stop the container with
```
$ docker stop my_snap

```

** Example **

Print the *snap* help:
```
$ docker run --rm bromberglab/snap --help
```

Run *snap* providing a fasta input file:
```
$ docker run --rm -v <LOCAL_INPUT_DIRECTORY>:/input -v <LOCAL_OUTPUT_DIRECTORY>:/output bromberglab/snap --seqfile query.fasta -o /output/snap.out
```
The file *query.fasta* has to be located in the directory <LOCAL_INPUT_DIRECTORY> mounted into the running container via the -v flag)

** Notes: **

Using the --rm argument, docker will delete the container once exited.

## Example ##

```
$ docker run --rm -v <LOCAL_DATA_DIRECTORY>:/usr/share/rostlab-data/data/ -v <PATH_TO_SNAP_GIT>/files:/input -v <PATH_TO_SNAP_GIT>/files/out:/output bromberglab/snap -i example/query.fasta -m example/mutations.txt -o /output/example/snap.out --workdir /output/example
```

## License ##

This project is licensed under a [NON COMMERCIAL SOFTWARE LICENSE AGREEMENT](http://www.bio-sof.com/snap).

## Citation ##

Please refer to [https://www.bromberglab.org/publication/yb-53-bromberg-2007/](https://www.bromberglab.org/publication/yb-53-bromberg-2007/).

## About ##

SNAP was developed by Yana Bromberg [BrombergLab](https://www.bromberglab.org/project/snap/) at the [RostLab](http://www.rostlab.org/). The docker image was created and is maintained by Maximilian Miller ([BrombergLab](http://www.bromberglab.org/)). Feel free to contact us for support (<services@bromberglab.org>).
