#!/usr/bin/env sh

# TODO: Set your configuration here.
run_executable() {
    job_in_base="$1"
    job_out_base="$2"

    executable="snapfun"
    fasta_in="query.fasta"
    mutfile_in="mutations.txt"

    cd "$job_out_base"
    "$executable" -i "$job_in_base/${fasta_in}" -m "$job_in_base/${mutfile_in}" \
                  -o "$job_out_base/snap.out" --workdir "$job_out_base" $args
}

args="$@"
base_path="$(pwd)"

input_path="${INPUT_PATH:-/input}"
output_path="${OUTPUT_PATH:-/output}"

input_path="$(cd "$(dirname "$input_path")"; pwd -P)/$(basename "$input_path")"
output_path="$(cd "$(dirname "$output_path")"; pwd -P)/$(basename "$output_path")"

cd "$input_path"

ls -1 | while read id; do
    [ -d "${output_path}" ] || mkdir "${output_path}"
    [ -d "${output_path}/${id}" ] || mkdir "${output_path}/${id}"
    run_executable "${input_path}/${id}" "${output_path}/${id}"
done

cd "$base_path"
